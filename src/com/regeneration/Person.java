package com.regeneration;
public class Person {
    private String name;
    public Wallet wallet;


    public Person(String name, Wallet wallet) {
        this.name = name;
        this.wallet = wallet;
    }

    public void purchase(double price) {
        double walletAmount = wallet.getAmount();
        if (walletAmount > price) {
            wallet.WithdrawMoney(price);
        } else
            System.out.println("not enough money");
    }
}

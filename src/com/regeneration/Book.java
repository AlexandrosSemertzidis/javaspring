package com.regeneration;
public class Book {
    public String title;
    private String author;
    private double price;

    public Book(){

    }

    public Book(String givenTitle, String givenAuthor,  double givenPrice){
        this.title = givenTitle;
        this.author = givenAuthor;
        this.price = givenPrice;

        System.out.println("Title: " + title + ", Author:  " + author + ", Price: " + price );
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}

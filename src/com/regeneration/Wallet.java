package com.regeneration;
public class Wallet {
    private double amount;

    public Wallet(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void DepositMoney(double N){
        amount += N;
    }

    public void WithdrawMoney(double N){
        amount -= N;
    }

}

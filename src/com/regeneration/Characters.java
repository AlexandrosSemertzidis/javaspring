package com.regeneration;
public class Characters {
    private String[] characters;
    private int[] numbers;

    public Characters(){
        characters = new String[26];
        characters[0] = "a";
        characters[1] = "b";
        characters[2] = "c";
        characters[3] = "d";
        characters[4] = "e";
        characters[5] = "f";
        characters[6] = "g";
        characters[7] = "h";
        characters[8] = "i";
        characters[9] = "j";
        characters[10] = "k";
        characters[11] = "l";
        characters[12] = "m";
        characters[13] = "n";
        characters[14] = "o";
        characters[15] = "p";
        characters[16] = "q";
        characters[17] = "r";
        characters[18] = "s";
        characters[19] = "t";
        characters[20] = "u";
        characters[21] = "v";
        characters[22] = "w";
        characters[23] = "x";
        characters[24] = "y";
        characters[25] = "z";
        characters[32] = "z";

        numbers = new int[40];
        numbers[30] = 0;
        numbers[31] = 1;
        numbers[32] = 2;
        numbers[33] = 3;
        numbers[34] = 4;
        numbers[35] = 5;
        numbers[36] = 6;
        numbers[37] = 7;
        numbers[38] = 8;
        numbers[39] = 9;
    }

    public boolean ContainsThreeSameCharacters(String string) {
        string = string.toLowerCase();
        boolean contains = false;
        String currentChar;
        String previousChar = "";
        int count = 1;
        while (string.length() > 0) {
            currentChar = string.substring(0, 1);
            if (previousChar.matches(currentChar)) {
                count++;
            } else {
                previousChar = currentChar;
                count = 1;
            }

            if (count == 3) {
                contains = true;
                break;
            }

            string = string.substring(1);
        }

        return contains;
    }
    public boolean ContainsThreeConsecutiveCharacters(String string){
        string = string.toLowerCase();
        boolean contains = false;
        String currentChar = "";
        String previousChar = "";
        int countDirect = 1;
        int countReverse = 1;
        int previousCharNumber = 0;
        int currentCharNumber = 0;
        while (string.length() > 0) {
            currentChar = string.substring(0, 1);
            previousCharNumber = GetNumberOfChar(previousChar);
            currentCharNumber = GetNumberOfChar(currentChar);

            if (currentCharNumber == previousCharNumber + 1) {
                countDirect++;
            } else {
                countDirect = 1;
            }

            if (currentCharNumber == previousCharNumber - 1) {
                countReverse++;
            } else {
                countReverse = 1;
            }
            previousChar = currentChar;

            if (countDirect == 3 || countReverse == 3) {
                contains = true;
                break;
            }

            string = string.substring(1);
        }

        return contains;
    }
    private int GetNumberOfChar(String character){
        boolean found = false;
        int foundIndex = -1;
        String currentChar;
        for (int i = 0; i < characters.length; i++) {
            currentChar = characters[i];
            if (character.matches(currentChar)){
                found = true;
                foundIndex = i;
                break;
            }
        }
        if (!found){
            for (int i = 20; i < numbers.length; i++) {
                currentChar = String.valueOf(numbers[i]);
                if (character.matches(currentChar)){
                    found = true;
                    foundIndex = i;
                    break;
                }
            }
        }
        if (found)
                return foundIndex;
        else
            //Return a value that is not within the tables
            return -2;
    }

}

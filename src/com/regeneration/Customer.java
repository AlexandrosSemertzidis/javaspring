package com.regeneration;
import javax.swing.*;

public class Customer {
    private String name;
    private double money;

    public Customer(String name, double money) {
        this.name = name;
        this.money = money;
    }

    public void purchaseBook(Book givenBook){
        double bookCost = givenBook.getPrice();
        if (money> bookCost){
            System.out.println(givenBook.getTitle() + "was bought");
            money = money - bookCost;
        }
        else
            System.out.println("not enough money");
        }
}

package com.regeneration;
public class SpecialCharacters {
    private String[] specialCharacters;

    public SpecialCharacters(){
        specialCharacters = new String[33];
        specialCharacters[0] = " ";
        specialCharacters[1] = "!";
        specialCharacters[2] = "\"";
        specialCharacters[3] = "#";
        specialCharacters[4] = "$";
        specialCharacters[5] = "%";
        specialCharacters[6] = "&";
        specialCharacters[7] = "'";
        specialCharacters[8] = "(";
        specialCharacters[9] = ")";
        specialCharacters[10] = "*";
        specialCharacters[11] = "+";
        specialCharacters[12] = ",";
        specialCharacters[13] = "-";
        specialCharacters[14] = ".";
        specialCharacters[15] = "/";
        specialCharacters[16] = ":";
        specialCharacters[17] = ";";
        specialCharacters[18] = "<";
        specialCharacters[19] = "=";
        specialCharacters[20] = ">";
        specialCharacters[21] = "?";
        specialCharacters[22] = "@";
        specialCharacters[23] = "[";
        specialCharacters[24] = "\\";
        specialCharacters[25] = "]";
        specialCharacters[26] = "^";
        specialCharacters[27] = "_";
        specialCharacters[28] = "`";
        specialCharacters[29] = "{";
        specialCharacters[30] = "}";
        specialCharacters[31] = "|";
        specialCharacters[32] = "~";
    }

    public String[] getSpecialCharacters() {
        return specialCharacters;
    }

    public void setSpecialCharacters(String[] specialCharacters) {
        this.specialCharacters = specialCharacters;
    }

    public boolean StringContainsSpecialCharacter(String string){
        boolean contains = false;
        for (int i = 0; i < specialCharacters.length; i++) {
            if (string.contains(specialCharacters[i])){
                contains=true;
                break;
            }
        }
        return contains;
    }

}

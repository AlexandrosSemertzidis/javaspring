package com.regeneration;
public class JavaExercises {

    //Java Syntax.1
    public int CalulateFactorial(int n){
        int result = 1;
        for (int x = 1; x <= n; x++) {
            result = result * x;
        }
        return result;
    }

    //Java Syntax.2
    public boolean IsPrimeNumber(int numberToCheck){
        boolean isPrime = true;
        for(int divisor = 2; divisor <= numberToCheck / 2; divisor++) {
            if (numberToCheck % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }

    //Java Syntax.3
    public double calulateSumOneOverNumber(int number){
        double result = 0;
        double tempNumber;
        for(int x=1; x<=number; x++){
            tempNumber = 1 /( (double) x);
            result += tempNumber;
        }
        return result;
    }

    public int countDigitsLongNumber(long number){
        int count = 0;
        while(number != 0){
            number = number / 10;
            count++;
        }
        return count;
    }

    public float getDecimalPart(float number){
        number = number % 1;
        return number;
    }

    public void CheckPassword(String password){
        boolean isValid = true;
        boolean mandatoryCriteria = true;
        int criteriaMet = 0;
        Boolean temp = false;
        String result;


        //1. Contains atleast one uppercase
        if (password == password.toLowerCase()){
            isValid = false;
        }
        else{
            criteriaMet++;
        }

        //2. Contains atleast one lowercase
        if (password == password.toUpperCase()){
            isValid = false;
        }
        else{
            criteriaMet++;
        }

        temp = false;
        //3. Contains atleast one number
        for (int x=0; x<=9; x++) {
            if (password.contains(String.valueOf(x))){
                temp=true;
                break;
            }
        }
        if (!temp) {
            isValid = false;
        }
        else{
            criteriaMet++;
        }

        //4. Contains at least one special character
        temp = false;
        SpecialCharacters specialCharacters = new SpecialCharacters();
        temp = specialCharacters.StringContainsSpecialCharacter(password);
        if (!temp) {
            isValid = false;
        }
        else{
            criteriaMet++;
        }

        //5. Length is at least 8 characters long
        if (password.length() < 8){
            isValid = false;
            mandatoryCriteria = false;
        }
        else{
            criteriaMet++;
        }

        //6. contain 3 same or 3 consecutive
        Characters characters = new Characters();
        if (characters.ContainsThreeSameCharacters(password) || characters.ContainsThreeConsecutiveCharacters(password)){
            isValid = false;
        }

        result = (mandatoryCriteria)? "valid" : "invalid";
        if (criteriaMet == 6){
            System.out.println("Very Strong Password");
        }
        else if (criteriaMet == 5){
            System.out.println("Strong Password");
        }
        else if (criteriaMet == 4 || criteriaMet == 3){
            System.out.println("Password OK");
        }
        else{
            System.out.println("Invalid Password");
            System.out.println("");
        }


    }



}

package com.regeneration;
public class Point {
    private int x;
    private int y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static double CalculateDistance(Point point1, Point point2, Point point3){
        double line1;
        double line2;
        double distance1;
        double distance2;
        double distance3;
        double totalDistance;

        line1 = Math.abs(point1.x - point2.x);
        line2 = Math.abs(point1.y - point2.y);
        distance1 = Math.sqrt(line1*line1 + line2*line2);

        line1 = Math.abs(point1.x - point3.x);
        line2 = Math.abs(point1.y - point3.y);
        distance2 = Math.sqrt(line1*line1 + line2*line2);

        line1 = Math.abs(point2.x - point3.x);
        line2 = Math.abs(point2.y - point3.y);
        distance3 = Math.sqrt(line1*line1 + line2*line2);

        totalDistance= distance1 + distance2 + distance3;
        return totalDistance;

    }
}
